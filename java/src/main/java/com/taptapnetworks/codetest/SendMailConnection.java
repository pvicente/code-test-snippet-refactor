package com.taptapnetworks.codetest;

public class SendMailConnection {

    public SendMailConnection(String mailServer) {
        super();
        System.out.println(String.format("Created SendMailConnection for server %s", mailServer));
    }

    public void prepareMessage(String to, String message, String subject) {
        System.out.println(String.format("Preparing message for %s", to));
        System.out.println(String.format("Subject: %s", subject));
        System.out.println(message);
    }

    public void send() {
        System.out.println("Message sent!!!");
    }

    public void close() {
        System.out.println("Mail connection closed");
    }
}
